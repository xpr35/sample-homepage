.. post:: Nov 22, 2017
   :tags: tag1, tag2
   :author: Author

Пост номер 1
============

И опять привет!!!!!! testtest

Снова привет.

Добро пожаловать на мастер-класс по разработке документации как кода.

Первый абзац поста будет использоваться как краткое содержание в архивах и RSS.
О том, как настроить отображение, читайте в `Post Excerpts and Images
<https://ablog.readthedocs.org/manual/post-excerpts-and-images/>`_.

Ссылки на посты можно делать по имени файла: из разметки ``:ref:`first-post``` получается ссылка :ref:`first-post`.
Подробнее это описано в документе `Cross-Referencing Blog Pages
<https://ablog.readthedocs.org/manual/cross-referencing-blog-pages/>`_.
